#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# System modules
import os
import runpy
from setuptools import setup, find_packages


def read_file(filename):
    with open(filename) as f:
        return f.read()


# run setup
setup(
    name="LICOR840A",
    description="Operate the LICOR 840A gas analyzer via a serial port",
    author="Yann Büchau",
    author_email="nobodyinperson@gmx.de",
    keywords="measurement,logging",
    license="GPLv3",
    version=runpy.run_path("LICOR840A/version.py").get("__version__", "0.0.0"),
    url="https://gitlab.com/tue-umphy/instruments/python3-LICOR840A",
    long_description=read_file("README.md"),
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.5",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    install_requires=["pyserial", "lxml"],
    packages=find_packages(),
    entry_points={"console_scripts": ["licor840a = LICOR840A.__main__:main"]},
)
