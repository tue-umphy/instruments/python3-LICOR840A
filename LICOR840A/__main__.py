# system modules
import argparse
import sys
import datetime
import os
import json
import csv
import signal
import re
import logging
import itertools

# external modules
import serial
import serial.tools.list_ports
import lxml.etree as etree

logger = logging.getLogger(os.path.basename(__file__))


def serial_device_file(arg=None):
    if os.path.exists(arg):
        try:
            return serial.Serial(arg)
        except serial.SerialException:
            raise argparse.ArgumentTypeError(
                "Cannot use '{}' as serial connection".format(arg)
            )
    else:
        raise argparse.ArgumentTypeError("'{}' does not exist".format(arg))


serial_ports = serial.tools.list_ports.comports()

parser = argparse.ArgumentParser(
    description="Access to LICOR-840a CO2/H2O gas analyzer"
)
parser.add_argument(
    "-p",
    "--port",
    help="serial device file",
    type=serial_device_file,
    default=serial_ports[0].device if len(serial_ports) == 1 else None,
    required=len(serial_ports) != 1,
)
parser.add_argument(
    "-b", "--baud", help="serial baudrate", required=False, default=None
)
parser.add_argument(
    "-v", "--verbose", help="verbose output", action="store_true"
)
parser.add_argument("-d", "--debug", help="debug output", action="store_true")
parser.add_argument(
    "-t", "--time_col", help="time column name", default="time_utc"
)
parser.add_argument(
    "-o",
    "--output",
    help="output file",
    type=argparse.FileType("w"),
    default=sys.stdout,
)


def main():
    args = parser.parse_args()

    loglevel = logging.WARNING
    loglevel = logging.INFO if args.verbose else loglevel
    loglevel = logging.DEBUG if args.debug else loglevel
    logging.basicConfig(level=loglevel)
    logger.level = loglevel

    if args.baud:
        args.port.baudrate = args.baud

    def xml2dict(xml):
        if xml.text:
            return xml.text
        else:
            return {e.tag: xml2dict(e) for e in xml}

    def flatten_dict(d):
        out = {}
        for k, v in d.items():
            if hasattr(v, "items"):
                fd = {
                    "{}_{}".format(k, i): j for i, j in flatten_dict(v).items()
                }
            else:
                fd = {k: v}
            out.update(fd)
        return out

    logger.info("Discarding any previous input from the device")
    args.port.flushInput()
    for line in args.port:
        line = line.decode(errors="ignore").rstrip()
        logger.info("read from device:\n{}".format(line))
        try:
            xml = etree.fromstring(line)
            logger.debug(
                "prettyfied XML:\n{}".format(
                    etree.tostring(xml, pretty_print=True).decode()
                )
            )
        except etree.XMLSyntaxError:
            logger.error(
                "could not parse XML. This is normal for the first line."
            )
            continue
        # turn XML into dict
        d = xml2dict(xml)
        logging.debug(
            "XML to dict:\n{}".format(json.dumps(d, indent=4, sort_keys=True))
        )
        # flatten the dict
        df = flatten_dict(d)
        logging.debug(
            "flattened dict:\n{}".format(
                json.dumps(df, indent=4, sort_keys=True)
            )
        )
        # add time
        df.update(
            {args.time_col: datetime.datetime.utcnow().strftime("%F %T")}
        )
        # write to output file
        try:
            csvwriter
        except NameError:
            csvwriter = csv.DictWriter(
                args.output,
                # put time column first
                fieldnames=sorted(
                    df.keys(),
                    key=lambda s: "0" + s
                    if s.startswith(args.time_col)
                    else s,
                ),
            )
            csvwriter.writeheader()
            args.output.flush()
        csvwriter.writerow(df)
        args.output.flush()


if __name__ == "__main__":
    main()
