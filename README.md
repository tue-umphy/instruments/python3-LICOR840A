# LICOR LI-840A data logging

## Installation

```bash
pip install git+https://gitlab.com/tue-umphy/instruments/python3-LICOR840A.git
```

## Usage

To log data from an LI-840A CO2/H2O gas analyzer connect it via the RS232-cable
and a USB-to-RS232 to your PC and run the following to dump the live CSV data:

```bash
licor840a
# time_utc,data_cellpres,data_celltemp,data_co2,data_co2abs,data_h2o,data_h2oabs,data_h2odewpoint,data_ivolt,data_raw_co2,data_raw_co2ref,data_raw_h2o,data_raw_h2oref
# 2022-01-20 09:26:12,9.58769e1,5.11997e1,5.14911e2,9.3592281e-2,2.75637,1.8844485e-2,-1.10662e1,1.5522460e1,2390313,2431750,1110477,1266442
# 2022-01-20 09:26:12,9.58560e1,5.12261e1,5.14909e2,9.3565702e-2,2.75807,1.8849015e-2,-1.10612e1,1.5529174e1,2390495,2432498,1110397,1266593
# ...

# if that says 'command not found' or something like that, try:
python3 -m LICOR840A
```

If the LI-COR840A is not the only serial device on your machine, you need to
specify the port:

```bash
licor840a -p /dev/ttyUSB1
```

To log to a file:

```bash
licor840a -o licor.csv

# or to also see the output:
licor840a | tee licor.csv
```
